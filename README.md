# Zephr Swift Client SDK

## Overview
The Zephr Swift Client SDK is a wrapper around the Zephr Public API, allowing client-side use in an Apple OS integration.
You can read more about the Public API here (https://developer.zephr.com/api-reference/).

## Installation
The Zephr Swift SDK can be added as a dependency of another Swift Package or as a Swift Package Dependency in Xcode.

To use this package, add the dependency `https://bitbucket.org/blaizeinc/zephr-saas-swift-client-sdk`

## Configuration
The Client SDK requires you to configure it with your organisation ID before API calls can be made.
Do this in your AppDelegate:
```swift
import UIKit
import ZephrClient

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        ZephrConfiguration.shared.orgId = "yourOrgId"
        // Override point for customization after application launch.
        return true
    }
```

It is also possible to specify a base URL for the Zephr service, if it is non-standard (not normally needed):
```swift
ZephrConfiguration.shared.overridePublicBaseUrl = "http://localhost:8090"
```

## SDK Usage
### Start Access Controller
```swift
import ZephrClient

func startAccessController() {
    ZephrClient.client.startAccessController(
        jwt: "...", // User authentication JWT
        path: "/", // Path of request
        inputs: ["custom": true] // Custom inputs key values
    ) { result in
        switch result {
        case .success:
            print("Zephr Access Controller started successfully")
        case .unauthorized:
            print("Session ID invalid or user is unauthorized")
        case .badRequest:
            print("Bad request inputs")
        case .serverError:
            print("Something went wrong")
        }
    }
}
```

### Update Access Controller
```swift
import ZephrClient

func startAccessController() {
    ZephrClient.client.updateAccessController(
        jwt: "...", // User authentication JWT
        path: "/", // Path of request
        inputs: ["custom": true] // Custom inputs key values
    ) { result in
        switch result {
        case .success:
            print("Zephr Access Controller updated successfully")
        case .unauthorized:
            print("Session ID invalid or user is unauthorized")
        case .badRequest:
            print("Bad request inputs")
        case .serverError:
            print("Something went wrong")
        }
    }
}
```

### On Access Controller Update
```swift
import ZephrClient

func onUpdate() {
    // Sometime after the access controller started
    // ...

    ZephrClient.client.onUpdate {
        print("Do something after update")
    }
    
    // onUpdate will fire after calling ZephrClient.client.updateAccessController(...)
}
```

### Check if a feature is enabled for the current user
```swift
import ZephrClient

func isFeatureEnabled() {
    ZephrClient.client.isFeatureEnabled(
        "...", // The feature slug to check
        inputs: ["custom": true], // Custom inputs for the decision engine
        defaultEnabled: false // Is the feature enabled by default
    ) { enabled in
        if enabled {
            print("Feature enabled")
        } else {
            print("Feature disabled")
        }
    }
}
```

### Get a rule decision details
```swift
import ZephrClient

func getRuleDecision() {

    ZephrClient.client.getRuleDecisionDetails(
        "...", // The rule slug to check
        inputs: ["custom": true], // Custom inputs for the decision engine
    ) { result in
        print("Do something with result")
    }
}
```

### Get a rule decision output value as string/int/double
```swift
import ZephrClient

func getRuleDecision() {

    // ZephrClient.client.getRuleDecisionString
    // ZephrClient.client.getRuleDecisionInt
    // ZephrClient.client.getRuleDecisionDouble
    ZephrClient.client.getRuleDecisionString(
        "...", // The rule slug to check
        inputs: ["custom": true], // Custom inputs for the decision engine
    ) { outputValue in
        print("Do something with \(outputValue)")
    }
}
```
