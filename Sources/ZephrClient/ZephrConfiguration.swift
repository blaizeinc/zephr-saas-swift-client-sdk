//
//  ZephrConfiguration.swift
//  ZephrClient
//
//  Created by Ben Kennedy on 24/11/2021.
//  Copyright © 2021 Zephr. All rights reserved.
//

import Foundation

/// Zephr Configuration
///
/// Creates a singleton instance for storing configuration of the Zephr Client
public class ZephrConfiguration {
    
    public static let shared = ZephrConfiguration()

    public var orgId: String?
    public var overridePublicBaseUrl: URL?

    init() {}
    
    /// Get the base URL for the Public API
    ///
    /// Uses the `orgId` to generate a base URL for the Public API.
    /// 
    /// Will return the `overridePublicBaseUrl` value if set.
    ///
    /// - Returns: The Public API base URL
    public func getPublicBaseUrl() -> URL? {
        
        if (overridePublicBaseUrl != nil) {
            return overridePublicBaseUrl!
        }

        guard let orgId = orgId else {
            return nil
        }

        return URL(string: "https://\(orgId).saas-public-api.zephr.com")
    }

}
