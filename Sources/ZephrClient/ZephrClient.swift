//
//  ZephrClient.swift
//  ZephrClient
//
//  Created by Ben Kennedy on 24/11/2021.
//  Copyright © 2021 Zephr. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import OSLog

let logger = Logger(subsystem: "com.zephr.saas.ZephrClient", category: "main")

/// Zephr Client
///
/// Creates a singleton client for controlling app flow using the access controller and decision engine.
public class ZephrClient {

    public static let client = ZephrClient()

    init() {
        session = Session(eventMonitors: [NetworkLogger()])
        accessParams = AccessParams()
        updateCallbacks = []
    }

    private(set) public var hasStarted = false
    private var accessParams: AccessParams
    private var accessState: AccessState? = nil
    private var updateCallbacks: [() -> Void]

    var session: Session
    
    /// Start Zephr access controller
    ///
    /// The Zephr access controller determines the current access state of a user
    ///
    /// - Parameters:
    ///   - jwt: Secure user JWT token
    ///   - path: URL path of initial page
    ///   - inputs: Custom inputs for the decision engine
    ///   - completionHandler: Invoked when starting the access controller failed or completed
    public func startAccessController(
        jwt: String,
        path: String = "/",
        inputs: Dictionary<String, Any>? = nil,
        completionHandler: @escaping (AccessControllerResponse) -> Void
    ) {
        accessParams.jwt = jwt
        accessParams.path = path

        let payload = generatePayload(inputs)

        hasStarted = true
        currentAccessStateRequest(payload) { result in
            self.hasStarted = false

            switch result {
            case .success(let response):
                self.accessState = response
                self.hasStarted = true
                completionHandler(.success)
            case .unauthorized:
                logger.error("JWT invalid or user is unauthorized")
                completionHandler(.unauthorized)
            case .badRequest:
                logger.error("Bad request inputs")
                completionHandler(.badRequest)
            case .serverError:
                logger.error("Something went wrong")
                completionHandler(.serverError)
            }
        }
    }
    
    /// Update Zephr access controller
    ///
    /// Update Zephr access controller when user state has changed e.g. logged in/out
    ///
    /// - Parameters:
    ///   - jwt: Secure user JWT token
    ///   - path: URL path of initial page
    ///   - inputs: Custom inputs for the decision engine
    ///   - completionHandler: Invoked when updating the access controller failed or completed
    public func updateAccessController(
        jwt: String? = nil,
        path: String? = nil,
        inputs: Dictionary<String, Any>? = nil,
        completionHandler: @escaping (AccessControllerResponse) -> Void
    ) {
        if !hasStarted {
            logger.error("Access Controller is not started")
            completionHandler(.badRequest)
            return
        }

        if jwt != nil {
            accessParams.jwt = jwt
        }
        if path != nil {
            accessParams.path = path
        }

        let payload = generatePayload(inputs)

        currentAccessStateRequest(payload) { result in
            switch result {
            case .success(let response):
                self.accessState = response
                self.triggerOnUpdate()
                completionHandler(.success)
            case .unauthorized:
                logger.error("JWT invalid or user is unauthorized")
                completionHandler(.unauthorized)
            case .badRequest:
                logger.error("Bad request inputs")
                completionHandler(.badRequest)
            case .serverError:
                logger.error("Something went wrong")
                completionHandler(.serverError)
            }
        }
    }
    
    /// Access controller update listener
    ///
    /// - Parameter completionHandler: Invoked when the user access state has updated
    public func onUpdate(completionHandler: @escaping () -> Void) {
        self.updateCallbacks.append(completionHandler)
    }
    
    /// Check if a feature is enabled for the current user
    ///
    /// - Parameters:
    ///   - slug: The feature slug to check
    ///   - inputs: Custom inputs for the decision engine
    ///   - defaultEnabled: Is the feature enabled by default
    ///   - completionHandler: Invoked when the feature state is returned
    public func isFeatureEnabled(
        _ slug: String,
        inputs: Dictionary<String, Any>? = nil,
        defaultEnabled: Bool = false,
        completionHandler: @escaping (Bool) -> Void
    ) {
        guard let feature = self.accessState?.features[slug] else {
            completionHandler(defaultEnabled)
            return
        }

        switch feature.type {
        case .STATIC:
            completionHandler(feature.enabled ?? defaultEnabled)
        case .DYNAMIC:
            let payload = generatePayload(inputs)

            featureAccessRequest(slug, payload) { result in
                switch result {
                case .success(let response):
                    completionHandler(response.enabled)
                default:
                    completionHandler(defaultEnabled)
                }
            }
        }
    }
    
    /// Get a rule decision
    ///
    /// - Parameters:
    ///   - slug: The rule slug to check
    ///   - inputs: Custom inputs for the decision engine
    ///   - completionHandler: Invoked when the rule decision is returned
    public func getRuleDecisionDetails(
        _ slug: String,
        inputs: Dictionary<String, Any>? = nil,
        completionHandler: @escaping (RuleDecisionResponse) -> Void
    ) {
        let payload = generatePayload(inputs)

        ruleDecisionRequest(slug, payload) { result in
            completionHandler(result)
        }
    }
    
    /// Get a rule decision with a string value
    ///
    /// - Parameters:
    ///   - slug: The rule slug to check
    ///   - inputs: Custom inputs for the decision engine
    ///   - completionHandler: Invoked when the rule decision is returned
    public func getRuleDecisionString(
        _ slug: String,
        inputs: Dictionary<String, Any>? = nil,
        completionHandler: @escaping (String?) -> Void
    ) {
        getRuleDecisionValue(slug: slug, outputTypes: [.STRING, .ENUM], inputs: inputs) { outputValue in
            guard let stringValue = outputValue as? String else {
                completionHandler(nil)
                return
            }

            completionHandler(stringValue)
        }
    }
    
    /// Get a rule decision with an integer value
    ///
    /// - Parameters:
    ///   - slug: The rule slug to check
    ///   - inputs: Custom inputs for the decision engine
    ///   - completionHandler: Invoked when the rule decision is returned
    public func getRuleDecisionInt(
        _ slug: String,
        inputs: Dictionary<String, Any>? = nil,
        completionHandler: @escaping (Int?) -> Void
    ) {
        getRuleDecisionValue(slug: slug, outputTypes: [.NUMBER], inputs: inputs) { outputValue in
            guard let doubleValue = outputValue as? Double else {
                completionHandler(nil)
                return
            }

            completionHandler(Int(doubleValue))
        }
    }
    
    /// Get a rule decision with a double value
    ///
    /// - Parameters:
    ///   - slug: The rule slug to check
    ///   - inputs: Custom inputs for the decision engine
    ///   - completionHandler: Invoked when the rule decision is returned
    public func getRuleDecisionDouble(
        _ slug: String,
        inputs: Dictionary<String, Any>? = nil,
        completionHandler: @escaping (Double?) -> Void
    ) {
        getRuleDecisionValue(slug: slug, outputTypes: [.NUMBER], inputs: inputs) { outputValue in
            guard let doubleValue = outputValue as? Double else {
                completionHandler(nil)
                return
            }

            completionHandler(doubleValue)
        }
    }

    /// Get a rule decision with any value
    ///
    /// - Parameters:
    ///   - slug: The rule slug to check
    ///   - inputs: Custom inputs for the decision engine
    ///   - completionHandler: Invoked when the rule decision is returned
    public func getRuleDecisionValue(
        slug: String,
        outputTypes: [RuleDecisionOutputType],
        inputs: Dictionary<String, Any>? = nil,
        completionHandler: @escaping (Any?) -> Void
    ) {
        getRuleDecisionDetails(slug, inputs: inputs) { result in
            switch result {
            case .success(let ruleDecision):
                if outputTypes.contains(ruleDecision.outputType) {
                    completionHandler(ruleDecision.outputValue)
                } else {
                    completionHandler(nil)
                }
            default:
                completionHandler(nil)
            }
        }
    }
    
    /// Check if the current user is authenticated
    ///
    /// - Returns: Is the current user authenticated?
    public func isAuthenticated() -> Bool {
        return accessState?.authenticated ?? false
    }
    
    /// Returns all of the current users features
    ///
    /// - Returns: All of the current users features
    public func getAllFeatures() -> Dictionary<String, FeatureAccessState> {
        return accessState?.features ?? [:]
    }
    
    /// Returns all of the current users meters
    ///
    /// - Returns: All of the current users meters
    public func getAllMeters() -> Dictionary<String, String> {
        return accessState?.meters ?? [:]
    }
    
    /// Report a users tier change
    ///
    /// Uses the tier in the JWT token to track a users tier changing.
    ///
    /// - Parameters:
    ///   - previousTierSlug: The users previous tier slug
    ///   - completionHandler: Invoked when the reporting has failed or completed
    public func reportUserTierChange(_ previousTierSlug: String? = nil, completionHandler: @escaping (BasicResponse) -> Void) {
        var payload = JSON()

        if previousTierSlug != nil {
            payload["previousTierSlug"].string = previousTierSlug
        }

        reportUserTierChangeRequest(payload, completionHandler)
    }
    
    private func currentAccessStateRequest(_ payload: JSON, _ completionHandler: @escaping (CurrentAccessStateResponse) -> Void) {
        
        request(of: AccessState.self, path: "/access/currentState", method: .post, json: payload) { result in
            switch result {
            case .success(let response):
                completionHandler(.success(response.body))
            case .failure(let error):
                logger.error("\(error.localizedDescription)")
                if error.isExplicitlyCancelledError {
                    logger.error("No Org ID set")
                    completionHandler(.badRequest)
                } else if error.responseCode == 401 {
                    completionHandler(.unauthorized)
                } else if error.responseCode == 400 {
                    completionHandler(.badRequest)
                } else {
                    completionHandler(.serverError)
                }
            }
        }
    }
    
    private func featureAccessRequest(
        _ slug: String,
        _ payload: JSON,
        _ completionHandler: @escaping (FeatureAccessStateResponse) -> Void
    ) {

        request(of: FeatureAccessStateResult.self, path: "/access/feature/\(slug)", method: .post, json: payload) { result in
            switch result {
            case .success(let response):
                completionHandler(.success(response.body))
            case .failure(let error):
                logger.error("\(error.localizedDescription)")
                if error.isExplicitlyCancelledError {
                    logger.error("No Org ID set")
                    completionHandler(.badRequest)
                } else if error.responseCode == 401 {
                    completionHandler(.unauthorized)
                } else if error.responseCode == 400 {
                    completionHandler(.badRequest)
                } else {
                    completionHandler(.serverError)
                }
            }
        }
    }
    
    private func ruleDecisionRequest(
        _ slug: String,
        _ payload: JSON,
        _ completionHandler: @escaping (RuleDecisionResponse) -> Void
    ) {

        request(of: RuleDecision.self, path: "/access/decision/\(slug)", method: .post, json: payload) { result in
            switch result {
            case .success(let response):
                completionHandler(.success(response.body))
            case .failure(let error):
                logger.error("\(error.localizedDescription)")
                if error.isExplicitlyCancelledError {
                    logger.error("No Org ID set")
                    completionHandler(.badRequest)
                } else if error.responseCode == 401 {
                    completionHandler(.unauthorized)
                } else if error.responseCode == 400 {
                    completionHandler(.badRequest)
                } else {
                    completionHandler(.serverError)
                }
            }
        }
    }
    
    private func reportUserTierChangeRequest(
        _ payload: JSON,
        _ completionHandler: @escaping (BasicResponse) -> Void
    ) {

        request(of: EventReportResponse.self, path: "/eventReporting/userTierChange", method: .post, json: payload) { result in
            switch result {
            case .success:
                completionHandler(.success)
            case .failure(let error):
                logger.error("\(error.localizedDescription)")
                if error.isExplicitlyCancelledError {
                    logger.error("No Org ID set")
                }

                completionHandler(.failure)
            }
        }
    }

    private func request<T: Decodable>(
        of type: T.Type = T.self,
        path: String,
        method: HTTPMethod,
        json: JSON? = nil,
        completionHandler: @escaping (Result<ResponseDetails<T>, AFError>) -> Void
    ) {
        
        guard let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl() else {
            completionHandler(.failure(.explicitlyCancelled))
            return
        }

        guard let requestUrl = URL(string: "\(baseUrl.absoluteString)\(path)") else {
            completionHandler(.failure(.explicitlyCancelled))
            return
        }

        session.request(requestUrl,
            method: method,
            parameters: json,
            encoder: JSONParameterEncoder.default,
                        headers: getRequestHeaders(
                            jwt: self.accessParams.jwt
                        ))
            .validate()
            .responseDecodable(of: T.self) { response in
                switch response.result {
                case .success(let json):
                    let requestResponse = ResponseDetails(
                        headers: response.response?.headers ?? [],
                        statusCode: response.response?.statusCode ?? -1,
                        body: json)
                    completionHandler(.success(requestResponse))
                case .failure(let error):
                    completionHandler(.failure(error))
                }
            }
    }
    
    private func getRequestHeaders(jwt: String? = nil) -> HTTPHeaders {

        var headers = HTTPHeaders.default
        headers.add(.contentType("application/json"))
        headers.add(.accept("application/json"))
        
        if jwt != nil {
            headers.add(.authorization("Bearer \(jwt!)"))
        }
        
        return headers
    }
    
    private func generatePayload(_ inputs: [String : Any]?) -> JSON {
        var payload = inputs != nil ? JSON(inputs!) : JSON()
        payload["path"].string = accessParams.path
        return payload
    }

    private func triggerOnUpdate() {
        self.updateCallbacks.forEach { callback in callback() }
    }

}

public enum BasicResponse: Decodable {
    case success
    case failure
}

public enum AccessControllerResponse {
    case success
    case unauthorized
    case badRequest
    case serverError
}

private enum CurrentAccessStateResponse {
    case success(AccessState)
    case unauthorized
    case badRequest
    case serverError
}

private enum FeatureAccessStateResponse {
    case success(FeatureAccessStateResult)
    case unauthorized
    case badRequest
    case serverError
}

public enum RuleDecisionResponse {
    case success(RuleDecision)
    case unauthorized
    case badRequest
    case serverError
}

private struct ResponseDetails<T> {
    let headers: HTTPHeaders
    let statusCode: Int
    let body: T
}

private struct AccessParams {
    var jwt: String?
    var path: String?
}

public enum FeatureAccessStateType: String, Decodable {
    case STATIC
    case DYNAMIC
}

public struct FeatureAccessState: Decodable {
    let type: FeatureAccessStateType
    let enabled: Bool?
    let meters: [String]?
}

private struct AccessState: Decodable {
    let authenticated: Bool
    let features: Dictionary<String, FeatureAccessState>
    let meters: Dictionary<String, String>
}

private struct FeatureAccessStateResult: Decodable {
    let featureSlug: String
    let enabled: Bool
    let meters: Dictionary<String, String>
}

public enum RuleDecisionOutputType: String, Decodable {
    case STRING
    case NUMBER
    case ENUM
}

public enum RuleDecisionError: Error {
    case invalidOutputValue
}

public struct RuleDecision: Decodable {
    let ruleSlug: String
    let outputType: RuleDecisionOutputType
    let outputValue: Any
    
    enum CodingKeys: String, CodingKey {
        case ruleSlug
        case outputType
        case outputValue
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        ruleSlug = try container.decode(String.self, forKey: .ruleSlug)
        outputType = try container.decode(RuleDecisionOutputType.self, forKey: .outputType)
        switch outputType {
        case .STRING:
            outputValue = try container.decode(String.self, forKey: .outputValue)
        case .NUMBER:
            if let outputDouble = try? container.decode(Double.self, forKey: .outputValue)  {
                outputValue = outputDouble
            } else {
                throw RuleDecisionError.invalidOutputValue
            }
        case .ENUM:
            outputValue = try container.decode(String.self, forKey: .outputValue)
        }
    }
}

public struct EventReportResponse: Decodable {
    let ok: Bool
    let message: String
}


final class NetworkLogger: EventMonitor {
    let queue = DispatchQueue(label: "Zephr SaaS API")

    // Event called when any type of Request is resumed.
    func requestDidResume(_ request: Request) {
        logger.info("Resuming: \(request)")
    }
    
    // Event called whenever a DataRequest has parsed a response.
    func request<Value>(_ request: DataRequest, didParseResponse response: DataResponse<Value, AFError>) {
        logger.debug("Finished: \(response)")
    }
}
