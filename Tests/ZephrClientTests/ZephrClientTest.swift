//
//  ZephrClientTest.swift
//  ZephrClientTests
//
//  Created by Ben Kennedy on 24/11/2021.
//  Copyright © 2021 Zephr. All rights reserved.
//

import XCTest
@testable import ZephrClient
@testable import Alamofire
@testable import Mocker
import SwiftyJSON

class ZephrClientTest: XCTestCase {

    override func setUp() {

        let configuration = URLSessionConfiguration.af.default
        configuration.protocolClasses = [MockingURLProtocol.self]
        ZephrClient.client.self.session = Session(configuration: configuration)
    }
    
    override func tearDown() {
        ZephrConfiguration.shared.reset()
    }

    func testStartAccessControllerNoOrg() {
        let expectation = XCTestExpectation()

        ZephrClient.client.startAccessController(jwt: "") { result in
            switch result {
            case .badRequest:
                XCTAssert(true)
            default:
                XCTFail()
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5.0)
    }
    
    func testStartAccessControllerSuccess() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let jwt = "aaabbbccc"
        let path = "/test"
        let customInput = "testing123"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        var mock = Mock(url: baseUrl.appendingPathComponent("/access/currentState"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "authenticated": true,
                    "features": {
                        "testfeature1": {
                            "type": "STATIC",
                            "enabled": true
                        },
                        "testfeature2": {
                            "type": "DYNAMIC",
                            "meters": []
                        },
                        "testfeature3": {
                            "type": "STATIC",
                            "enabled": false
                        }
                    },
                    "meters": {}
                }
            """.data(using: .utf8)!
        ])
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
            XCTAssertEqual(request.headers.value(for: "Authorization"), "Bearer \(jwt)")
            XCTAssertEqual(json["path"].stringValue, path)
            XCTAssertEqual(json["custom"].stringValue, customInput)
            onRequestExpectation.fulfill()
        }
        mock.register()

        ZephrClient.client.startAccessController(jwt: jwt, path: path, inputs: ["custom": customInput]) { result in
            switch result {
            case .success:
                let features = ZephrClient.client.getAllFeatures()
                XCTAssertEqual(features.count, 3)
                XCTAssertEqual(features["testfeature1"]!.type, FeatureAccessStateType.STATIC)
                XCTAssertEqual(features["testfeature1"]!.enabled!, true)
                XCTAssertEqual(features["testfeature2"]!.type, FeatureAccessStateType.DYNAMIC)
                XCTAssertEqual(features["testfeature2"]!.enabled, nil)
                XCTAssertEqual(features["testfeature2"]!.meters!.count, 0)
                XCTAssertEqual(features["testfeature3"]!.type, FeatureAccessStateType.STATIC)
                XCTAssertEqual(features["testfeature3"]!.enabled!, false)
            default:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testStartAccessControllerUnauthorized() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let jwt = "aaabbbccc"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        let mock = Mock(url: baseUrl.appendingPathComponent("/access/currentState"), dataType: .json, statusCode: 401, data: [
            .post: """
                {
                    "status": 401,
                    "type": "UNAUTHORIZED",
                    "message": "Session ID is missing or invalid"
                }
            """.data(using: .utf8)!
        ])
        mock.register()

        ZephrClient.client.startAccessController(jwt: jwt) { result in
            switch result {
            case .unauthorized:
                XCTAssert(true)
            default:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }

    
    func testUpdateAccessControllerNotStarted() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"

        ZephrClient.client.updateAccessController { result in
            switch result {
            case .badRequest:
                XCTAssert(true)
            default:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testUpdateAccessControllerUnauthorized() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let jwt = ""
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/currentState"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "authenticated": true,
                    "features": {
                        "testfeature1": {
                            "type": "STATIC",
                            "enabled": true
                        }
                    },
                    "meters": {}
                }
            """.data(using: .utf8)!
        ]).register()

        ZephrClient.client.startAccessController(jwt: jwt) { result in
            switch result {
            case .success:
                XCTAssert(true)
                Mock(url: baseUrl.appendingPathComponent("/access/currentState"), dataType: .json, statusCode: 401, data: [
                    .post: """
                        {
                            "status": 401,
                            "type": "UNAUTHORIZED",
                            "message": "JWT is missing or invalid"
                        }
                    """.data(using: .utf8)!
                ]).register()
                
                ZephrClient.client.updateAccessController(jwt: jwt) { result in
                    switch result {
                    case .unauthorized:
                        XCTAssert(true)
                    default:
                        XCTFail()
                    }
                    finishExpectation.fulfill()
                }
            default:
                XCTFail()
            }
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }

    
    func testUpdateAccessControllerSuccess() {
        let finishExpectation = XCTestExpectation()
        let onUpdateExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let jwt = "aaabbbccc"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/currentState"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "authenticated": true,
                    "features": {
                        "testfeature1": {
                            "type": "STATIC",
                            "enabled": true
                        }
                    },
                    "meters": {}
                }
            """.data(using: .utf8)!
        ]).register()
        
        ZephrClient.client.onUpdate {
            onUpdateExpectation.fulfill()
        }

        ZephrClient.client.startAccessController(jwt: jwt) { result in
            switch result {
            case .success:
                XCTAssert(true)
                Mock(url: baseUrl.appendingPathComponent("/access/currentState"), dataType: .json, statusCode: 200, data: [
                    .post: """
                    {
                        "authenticated": true,
                        "features": {
                            "testfeature1": {
                                "type": "STATIC",
                                "enabled": true
                            },
                            "testfeature2": {
                                "type": "DYNAMIC",
                                "meters": []
                            }
                        },
                        "meters": {}
                    }
                    """.data(using: .utf8)!
                ]).register()
                
                ZephrClient.client.updateAccessController(jwt: jwt) { result in
                    switch result {
                    case .success:
                        XCTAssert(true)
                    default:
                        XCTFail()
                    }
                    finishExpectation.fulfill()
                }
            default:
                XCTFail()
            }
        }

        wait(for: [finishExpectation, onUpdateExpectation], timeout: 5.0)
    }
    
    func testIsFeatureEnabledDefault() {
        let finishExpectation = XCTestExpectation()

        ZephrClient.client.isFeatureEnabled("missing") { enabled in
            XCTAssertEqual(enabled, false)
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testIsFeatureEnabledDefaultOverridden() {
        let finishExpectation = XCTestExpectation()

        ZephrClient.client.isFeatureEnabled("missing", defaultEnabled: true) { enabled in
            XCTAssertEqual(enabled, true)
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testIsFeatureEnabledStaticFeature() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let jwt = "aaabbbccc"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/currentState"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "authenticated": true,
                    "features": {
                        "testfeature1": {
                            "type": "STATIC",
                            "enabled": true
                        }
                    },
                    "meters": {}
                }
            """.data(using: .utf8)!
        ]).register()

        ZephrClient.client.startAccessController(jwt: jwt) { result in
            switch result {
            case .success:
                ZephrClient.client.isFeatureEnabled("testfeature1") { enabled in
                    XCTAssertEqual(enabled, true)
                    finishExpectation.fulfill()
                }
            default:
                XCTFail()
            }
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testIsFeatureEnabledDynamicFeature() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let jwt = "aaabbbccc"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/currentState"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "authenticated": true,
                    "features": {
                        "testfeature": {
                            "type": "DYNAMIC",
                            "meters": []
                        }
                    },
                    "meters": {}
                }
            """.data(using: .utf8)!
        ]).register()
        
        Mock(url: baseUrl.appendingPathComponent("/access/feature/testfeature"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "featureSlug": "testfeature",
                    "enabled": true,
                    "meters": {}
                }
            """.data(using: .utf8)!
        ]).register()

        ZephrClient.client.startAccessController(jwt: jwt) { result in
            switch result {
            case .success:
                ZephrClient.client.isFeatureEnabled("testfeature") { enabled in
                    XCTAssertEqual(enabled, true)
                    finishExpectation.fulfill()
                }
            default:
                XCTFail()
            }
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testGetRuleDecisionDetailsNotFound() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/decision/test"), dataType: .json, statusCode: 404, data: [
            .post: """
                {
                    "status": 404,
                    "type": "NOT_FOUND",
                    "message": "rule not found"
                }
            """.data(using: .utf8)!
        ]).register()
        
        ZephrClient.client.getRuleDecisionDetails("test") { result in
            switch result {
            case .serverError:
                XCTAssert(true)
                finishExpectation.fulfill()
            default:
                XCTFail()
            }
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testGetRuleDecisionDetailsSuccess() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/decision/test"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "ruleSlug": "test",
                    "outputType": "STRING",
                    "outputValue": "test output"
                }
            """.data(using: .utf8)!
        ]).register()
        
        ZephrClient.client.getRuleDecisionDetails("test") { result in
            switch result {
            case .success(let ruleDecision):
                XCTAssertEqual(ruleDecision.ruleSlug, "test")
                XCTAssertEqual(ruleDecision.outputType, .STRING)
                XCTAssertEqual(ruleDecision.outputValue as? String, "test output")
                finishExpectation.fulfill()
                finishExpectation.fulfill()
            default:
                XCTFail()
            }
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testGetRuleDecisionStringSuccess() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/decision/test"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "ruleSlug": "test",
                    "outputType": "STRING",
                    "outputValue": "test output"
                }
            """.data(using: .utf8)!
        ]).register()
        
        ZephrClient.client.getRuleDecisionString("test") { outputValue in
            XCTAssertEqual(outputValue, "test output")
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testGetRuleDecisionStringWrongType() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/decision/test"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "ruleSlug": "test",
                    "outputType": "NUMBER",
                    "outputValue": 42
                }
            """.data(using: .utf8)!
        ]).register()
        
        ZephrClient.client.getRuleDecisionString("test") { outputValue in
            XCTAssertEqual(outputValue, nil)
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testGetRuleDecisionStringSuccessWithEnum() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/decision/test"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "ruleSlug": "test",
                    "outputType": "ENUM",
                    "outputValue": "OUTPUT"
                }
            """.data(using: .utf8)!
        ]).register()
        
        ZephrClient.client.getRuleDecisionString("test") { outputValue in
            XCTAssertEqual(outputValue, "OUTPUT")
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testGetRuleDecisionIntSuccess() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/decision/test"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "ruleSlug": "test",
                    "outputType": "NUMBER",
                    "outputValue": 42
                }
            """.data(using: .utf8)!
        ]).register()
        
        ZephrClient.client.getRuleDecisionInt("test") { outputValue in
            XCTAssertEqual(outputValue, 42)
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testGetRuleDecisionIntActuallyDouble() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/decision/test"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "ruleSlug": "test",
                    "outputType": "NUMBER",
                    "outputValue": 42.1
                }
            """.data(using: .utf8)!
        ]).register()
        
        ZephrClient.client.getRuleDecisionInt("test") { outputValue in
            XCTAssertEqual(outputValue, 42)
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testGetRuleDecisionDoubleSuccess() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/decision/test"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "ruleSlug": "test",
                    "outputType": "NUMBER",
                    "outputValue": 42.1
                }
            """.data(using: .utf8)!
        ]).register()
        
        ZephrClient.client.getRuleDecisionDouble("test") { outputValue in
            XCTAssertEqual(outputValue, 42.1)
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testGetRuleDecisionDoubleActuallyInt() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/decision/test"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "ruleSlug": "test",
                    "outputType": "NUMBER",
                    "outputValue": 42
                }
            """.data(using: .utf8)!
        ]).register()
        
        ZephrClient.client.getRuleDecisionDouble("test") { outputValue in
            XCTAssertEqual(outputValue, 42.0)
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testReportUserTierChangeSuccess() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let jwt = "aaabbbccc"
        let previousTierSlug = "free"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/currentState"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "authenticated": true,
                    "features": {},
                    "meters": {}
                }
            """.data(using: .utf8)!
        ]).register()

        var mock = Mock(url: baseUrl.appendingPathComponent("/eventReporting/userTierChange"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "ok": true,
                    "message": "Event received successfully. However no conversions were attributed"
                }
            """.data(using: .utf8)!
        ])
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
            XCTAssertEqual(request.headers.value(for: "Authorization"), "Bearer \(jwt)")
            XCTAssertEqual(json["previousTierSlug"].string, previousTierSlug)
            onRequestExpectation.fulfill()
        }
        mock.register()
        
        ZephrClient.client.startAccessController(jwt: jwt) { result in
            switch result {
            case .success:
                ZephrClient.client.reportUserTierChange(previousTierSlug) { result in
                    XCTAssertEqual(result, .success)
                    finishExpectation.fulfill()
                }
            default:
                XCTFail()
            }
        }
    }

    func testReportUserTierChangeFailure() {
        let finishExpectation = XCTestExpectation()

        ZephrConfiguration.shared.orgId = "test"
        let jwt = "aaabbbccc"
        let baseUrl = ZephrConfiguration.shared.getPublicBaseUrl()!
        Mock(url: baseUrl.appendingPathComponent("/access/currentState"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                    "authenticated": true,
                    "features": {},
                    "meters": {}
                }
            """.data(using: .utf8)!
        ]).register()

        Mock(url: baseUrl.appendingPathComponent("/eventReporting/userTierChange"), dataType: .json, statusCode: 401, data: [
            .post: """
                {
                    "status": 401,
                    "type": "UNAUTHORIZED",
                    "message": "Session ID is missing or invalid"
                }
            """.data(using: .utf8)!
        ]).register()
        
        ZephrClient.client.startAccessController(jwt: jwt) { result in
            switch result {
            case .success:
                ZephrClient.client.reportUserTierChange() { result in
                    XCTAssertEqual(result, .failure)
                    finishExpectation.fulfill()
                }
            default:
                XCTFail()
            }
        }

    wait(for: [finishExpectation], timeout: 5.0)
}
}
