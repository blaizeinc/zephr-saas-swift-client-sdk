//
//  ZephrConfigurationTest.swift
//  ZephrClientTests
//
//  Created by Ben Kennedy on 24/11/2021.
//  Copyright © 2021 Zephr. All rights reserved.
//

import XCTest
@testable import ZephrClient

class ZephrConfigurationTest: XCTestCase {

    override func tearDown() {
        ZephrConfiguration.shared.reset()
    }

    func testInit() {
        let sharedConfig = ZephrConfiguration.shared;
        XCTAssertNil(sharedConfig.orgId)
        XCTAssertNil(sharedConfig.overridePublicBaseUrl)
    }

    func testSharedConfig() {
        ZephrConfiguration.shared.orgId = "testOrg"
        XCTAssertEqual(ZephrConfiguration.shared.orgId, "testOrg")
        
        ZephrConfiguration.shared.overridePublicBaseUrl = URL(string: "http://localhost:8080")
        XCTAssertEqual(ZephrConfiguration.shared.overridePublicBaseUrl, URL(string: "http://localhost:8080"))
    }

    func testGetPublicUrlNil() {
        XCTAssertEqual(ZephrConfiguration.shared.getPublicBaseUrl(), nil)
    }

    func testGetPublicUrlOrg() {
        ZephrConfiguration.shared.orgId = "test-org"
        XCTAssertEqual(ZephrConfiguration.shared.getPublicBaseUrl(), URL(string: "https://test-org.saas-public-api.zephr.com"))
    }

    func testGetPublicUrlOverride() {
        ZephrConfiguration.shared.overridePublicBaseUrl = URL(string: "http://localhost:8080")
        XCTAssertEqual(ZephrConfiguration.shared.getPublicBaseUrl(), URL(string: "http://localhost:8080"))
    }

}

extension ZephrConfiguration {
    func reset() {
        orgId = nil
        overridePublicBaseUrl = nil
    }
}
